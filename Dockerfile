FROM hseeberger/scala-sbt:graalvm-ce-19.3.0-java11_1.3.7_2.13.1 as build
COPY . /aus-configuration-service
WORKDIR /aus-configuration-service
RUN sbt assembly

FROM openjdk:jre-alpine
COPY --from=build /aus-configuration-service/target/scala-2.13/aus-configuration-service*.jar /aus-configuration-service/aus-configuration-service.jar
EXPOSE 8080
CMD java -jar /aus-configuration-service/aus-configuration-service.jar
