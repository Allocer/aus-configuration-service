val http4sVersion = "0.21.1"
val circeVersion = "0.13.0"
val zioVersion = "1.0.0-RC17"
val logbackVersion = "1.2.3"
val zioConfigVersion = "1.0.0-RC11"
val pureConfigVersion = "0.12.2"

addCompilerPlugin(dependency = "org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)
addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")

lazy val excludes = jacocoExcludes in Test := Seq(
  "com.aureus.configuration.service.main.Main*"
)

lazy val jacoco = jacocoReportSettings in test := JacocoReportSettings(
  "Jacoco Coverage Report: aus-configuration-service",
  None,
  JacocoThresholds(branch = 100),
  Seq(JacocoReportFormats.ScalaHTML, JacocoReportFormats.CSV), "utf-8")

val jacocoSettings = Seq(jacoco, excludes)

lazy val root = (project in file("."))
  .settings(
    organization := "gniewomir.gardas",
    name := "aus-configuration-service",
    scalaVersion := "2.13.1",
    version := "latest",
    assemblyJarName in assembly := "aus-configuration-service.jar",
    mainClass in assembly := Some("com.aureus.configuration.service.main.Main"),
    jacocoSettings,
    commonSettings,
  )
  .enablePlugins(AssemblyPlugin)

lazy val commonSettings = Seq(
  libraryDependencies ++= Seq(
    "dev.zio" %% "zio" % zioVersion,
    "dev.zio" %% "zio-interop-cats" % "2.0.0.0-RC10",
    "dev.zio" %% "zio-test" % zioVersion % "test",
    "dev.zio" %% "zio-test-sbt" % zioVersion % "test",
    "dev.zio" %% "zio-logging-slf4j" % "0.2.1",
    "org.http4s" %% "http4s-blaze-server" % http4sVersion,
    "org.http4s" %% "http4s-circe" % http4sVersion,
    "org.http4s" %% "http4s-dsl" % http4sVersion,
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-generic-extras" % circeVersion,
    "ch.qos.logback" % "logback-classic" % logbackVersion % Runtime,
    "com.github.pureconfig" %% "pureconfig" % pureConfigVersion,
  ),
  testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")
)
