import zio.ZIO
import zio.test._
import zio.test.Assertion._

object TestMain extends DefaultRunnableSpec(
  suite("TestMain")(
    testM("should return true for given numbers") {
      ZIO.succeed(assert(2 < 3, equalTo(true)))
    }
  )
)