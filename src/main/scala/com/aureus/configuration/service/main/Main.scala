package com.aureus.configuration.service.main

import http.HealthCheckRoutes
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze._
import zio._
import zio.interop.catz._
import zio.interop.catz.implicits._
import com.aureus.configuration.service.config._

object Main extends App {

  val routes = Router[Task](
    "/" -> HealthCheckRoutes.routes
  ).orNotFound

  val server: ZIO[ZEnv, Throwable, Unit] = ZIO.runtime[ZEnv]
    .flatMap {
      implicit rts =>
        BlazeServerBuilder[Task]
          .bindHttp(config.port, config.host)
          .withHttpApp(routes)
          .serve
          .compile
          .drain
    }

  def run(args: List[String]): URIO[ZEnv, Int] = {
    server.fold(_ => 1, _ => 0)
  }
}
