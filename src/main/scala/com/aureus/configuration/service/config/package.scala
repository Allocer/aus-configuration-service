package com.aureus.configuration.service

import pureconfig.ConfigSource
import pureconfig.generic.auto._

package object config {

  final case class Config(config: ApiConfig)

  final case class ApiConfig(port: Int, host: String)

  def config: ApiConfig = ConfigSource.default.loadOrThrow[Config].config
}
