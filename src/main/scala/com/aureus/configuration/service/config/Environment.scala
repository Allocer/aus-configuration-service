package com.aureus.configuration.service.config

import zio.ZIO
import zio.clock.Clock
import zio.logging.Logging

object Environment {
  type AppEnv = Clock with Logging
  type AppIO[T] = ZIO[AppEnv, Throwable, T]
}
