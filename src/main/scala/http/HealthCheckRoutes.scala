package http

import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import zio.Task
import zio.interop.catz._

object HealthCheckRoutes extends Http4sDsl[Task] {
  def routes: HttpRoutes[Task] = HttpRoutes.of[Task] {
    case GET -> Root / "meta" / "health" => Ok("Server is up")
  }
}
